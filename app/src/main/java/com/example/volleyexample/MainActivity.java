package com.example.volleyexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.volleyexample.Configs.ApiUrl;
import com.example.volleyexample.Utils.NetworkHelper;
import com.example.volleyexample.Utils.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    TextView txtGetData, txtPostData;
    Button buttonGetData, buttonPostData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        configureLayout();
    }

    private void configureLayout() {
        txtGetData = findViewById(R.id.txtGetData);
        txtPostData = findViewById(R.id.txtPostData);
        buttonGetData = findViewById(R.id.btnGet);
        buttonPostData = findViewById(R.id.btnPost);

        buttonGetData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTodo();
            }
        });

        buttonPostData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postTodo();
            }
        });
    }

    private void getTodo() {
        Log.i("TAG", "getTodo: ");
        NetworkHelper getTodoRequest = new NetworkHelper(Request.Method.GET, ApiUrl.TODO_URL, null, null, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("onResponse", response);
                txtGetData.setText(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("VolleyError", error.toString());
            }
        });
        VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(getTodoRequest);
    }

    private void postTodo() {
        try{
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("userId", 1);
            jsonBody.put("title", "test post");
            final String mRequestBody = jsonBody.toString();

            NetworkHelper postTodoRequest = new NetworkHelper(Request.Method.POST, ApiUrl.POST_TODO_URL, mRequestBody,null, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("onResponse", response);
                   txtPostData.setText(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Log.e("onErrorResponse", error.toString());

                }
            });

            VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(postTodoRequest);


        }
        catch (JSONException e){

        }
        
    }
}
