package com.example.volleyexample.Configs;

public class ApiUrl {
    private static final String ROOT_URL = "https://jsonplaceholder.typicode.com";
    public static final String TODO_URL = ROOT_URL + "/todos/1";
    public static final String POST_TODO_URL = ROOT_URL + "/todos";
}
