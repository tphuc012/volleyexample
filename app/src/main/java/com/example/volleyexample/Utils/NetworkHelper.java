package com.example.volleyexample.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;

public class NetworkHelper extends StringRequest {
    String mRequestBody;
    Map<String, String> header;

    public NetworkHelper(int method,
                         String url,
                         @Nullable String mRequestBody,
                         @Nullable Map<String, String> header,
                         Response.Listener<String> listener,
                         @Nullable Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        this.mRequestBody = mRequestBody;
        this.header = header;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        if (this.header != null) {
            return header;
        }
        Map<String, String> params = new HashMap<String, String>();
        params.put("Content-Type", "application/json; charset=UTF-8");
        return params;
    }

    @Override
    public String getBodyContentType() {
        return "application/json; charset=utf-8";
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
            return null;
        }
    }
}
